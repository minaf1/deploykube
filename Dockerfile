#FROM maven:3-jdk-8-alpine

#WORKDIR /usr/src/app

#COPY . /usr/src/app
#RUN mvn package

#ENV PORT 5000
#EXPOSE $PORT
#CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]
FROM tomcat:8.5.46-jdk8-openjdk as build-env
COPY /target/*.war $CATALINA_HOME/webapps/